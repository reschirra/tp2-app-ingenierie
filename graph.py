import igraph as ig
import matplotlib.pyplot as plt
from math import exp

def lire_fichier_et_separer(path):
    liste1 = []
    liste2 = []
    
    # Ouvrir et lire le fichier
    with open(path, 'r') as fichier:
        for ligne in fichier:
            ligne = ligne.strip()
            if '->' in ligne:
                a, b = ligne.split(' -> ')
                liste1.append(float(a.strip()))
                liste2.append(float(b.strip()))
    return liste1, liste2

def trier(x,y):
    for i in range(0,len(x)):
        for j in range(i,len(x)):
            if x[j] < x[i]:
                x[j], x[i] = x[i], x[j]
                y[j], y[i] = y[i], y[j]

plt.figure(figsize=(9,7), dpi=100)
x = []
y = []
x2,y2 = lire_fichier_et_separer("result1.txt")
x3,y3 = lire_fichier_et_separer("result2.txt")
x4,y4 = lire_fichier_et_separer("tcheb1.txt")
x5,y5 = lire_fichier_et_separer("tcheb2.txt")
x6,y6 = lire_fichier_et_separer("result3.txt")
x7,y7 = lire_fichier_et_separer("result4.txt")

x8,y8 = lire_fichier_et_separer("pointsdrt.txt")
trier(x8,y8)
x9, y9 = [], []
x10, y10 = lire_fichier_et_separer("pointsexp.txt")
x11, y11 = [], []
x12, y12 = lire_fichier_et_separer("pointspuissance.txt")
x13, y13 = [], []

with open("drt.txt", 'r') as fichier:
    for ligne in fichier:
        ligne = ligne.strip()
        if '&' in ligne:
            a1, a0 = ligne.split(' & ')
            a0 = float(a0.strip())
            a1 = float(a1.strip())

for i in range (int(min(x8)),int(max(x8))+1,1):
    x9.append(i)
    y9.append(i*a0+a1)

with open("exp.txt", 'r') as fichier:
    for ligne in fichier:
        ligne = ligne.strip()
        if '&' in ligne:
            a1, a0 = ligne.split(' & ')
            a0 = float(a0.strip())
            a1 = float(a1.strip())

for i in range (int(min(x10)),int(max(x10))+1,1):
    x11.append(i)
    y11.append(exp(a1) *(exp(i*a0)))

with open("puissance.txt", 'r') as fichier:
    for ligne in fichier:
        ligne = ligne.strip()
        if '&' in ligne:
            a1, a0 = ligne.split(' & ')
            a0 = float(a0.strip())
            a1 = float(a1.strip())

for i in range (int(x12[0]),int(x12[-1])+1,20):
    x13.append(i)
    y13.append(exp(a1)*(i**a0))

for i in range(-1,2):
    x.append(i)
    y.append(abs(i))

## Affichage des droites 
plt.plot(x,y,'go-') 
# plt.plot(x2,y2,'bo-') 
# plt.plot(x3,y3,'ro-')
plt.plot(x4,y4,'bo-')
plt.plot(x5,y5,'ro-')
# plt.plot(x6,y6,'bo-')
# plt.plot(x7,y7,'ro-')
# plt.plot(x12,y12,'go-')
# plt.plot(x13,y13,'ko-')
plt.xlabel("X")
plt.ylabel("Y")
plt.show()