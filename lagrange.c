#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.141592653589793238


// Calcule en x, avec le tableau de xs et ys, de taille n
float lagrange(float x, float* xs, float* ys, int n) {
    float sum = 0;
    for (int i = 0; i < n; i++) { // la somme
        float mult = 1;
        for (int j = 0; j < n; j++) { // la multiplication
            if (j != i) {
                mult *= (x-xs[j]) / (xs[i]-xs[j]);
            }
        }
        sum += ys[i] * mult;
    }
    return sum;
}

float tchebytchev(float a, float b, float i, int n) {
    float v1 = (a+b)/2;
    float v2 = (b-a)/2;
    float v3 = (2 * (n-i) + 1) * PI;
    v3 = v3 / (2 * (n + 1));
    v3 = cos(v3);
    v2 = v2 * v3;
    return fabs(v1 + v2);
}

void graph(float (*fun)(float, float*, float*, int), float* xs, float* ys, int n, float step, int a, int b) {
    for (float i=a; i<b; i+=step) {
        float v = fun(i, xs, ys, n);
        for(int k=0; k<v*step; k++) {
            printf(" ");
        }
        printf("*\n");
    }
}


int main(int argc, char const *argv[]) {
    float xs1[5] = {-1, -0.5, 0, 0.5, 1};
    float ys1[5] = {1, 0.5, 0, 0.5, 1};

    float xs2[11] = {-1, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1};
    float ys2[11] = {1, 0.8, 0.6, 0.4, 0.2, 0, 0.2, 0.4, 0.6, 0.8, 1};

    float xs3[5];
    float ys3[5];
    float xs4[11];
    float ys4[11];

    int n = 4;
    for(int i=0; i<n+1;++i){
        xs3[i] = i*(2.0/n) - 1;
        ys3[i] = tchebytchev(-1.0, 1.0, i, n);
    }
    n = 10;
    for(int i=0; i<n+1;++i){
        xs4[i] = i*(2.0/n) - 1;
        ys4[i] = tchebytchev(-1.0, 1.0, i, n);
    }


    FILE* fd1 = fopen("result1.txt", "w");
    FILE* fd2 = fopen("result2.txt", "w");
    FILE* fd3 = fopen("tcheb1.txt", "w");
    FILE* fd4 = fopen("tcheb2.txt", "w");
    FILE* fd5 = fopen("result3.txt", "w");
    FILE* fd6 = fopen("result4.txt", "w");


    
    for(float i=-1; i<=1.1; i+=0.1) {
        fprintf(fd1, "%f -> %f\n", i, lagrange(i, xs1, ys1, 5));
        fprintf(fd2, "%f -> %f\n", i, lagrange(i, xs2, ys2, 11));
        fprintf(fd5, "%f -> %f\n", i, lagrange(i, xs3, ys3, 5));
        fprintf(fd6, "%f -> %f\n", i, lagrange(i, xs4, ys4, 11));
    }

    for (int i=0; i<5; i++) {
        fprintf(fd3, "%f -> %f\n", xs3[i], ys3[i]);
    }
    for (int i=0; i<11; i++) {
        fprintf(fd4, "%f -> %f\n", xs4[i], ys4[i]);
    }
    fclose(fd1);
    fclose(fd2);
    fclose(fd3);
    fclose(fd4);
    
    return 0;
}
