#include <stdlib.h>
#include <stdio.h>
#include <math.h>


void ecriture_droite(char* txt1,char* txt2,float* x, float* y,float a0, float a1, int n){
    FILE* fd1 = fopen(txt1, "w");
    FILE* fd2 = fopen(txt2, "w");
    fprintf(fd1, "%f & %f\n",a0,a1);
    for(int i=0; i<n;++i){
        fprintf(fd2, "%f -> %f\n",x[i],y[i]);
    }
    fclose(fd1);
    fclose(fd2);
}

void calcul_droite(float* x,float* y,int n, float *a0, float *a1){
    // Cette fonction sert à calculer les valeurs de a0 et a1 selon une droite de régression
    float x_bar = 0;
    float y_bar = 0;
    float xy_bar = 0;
    float x_bar_carre = 0;
    for(int i=0; i<n;++i){
        x_bar += x[i];
        y_bar += y[i];
        xy_bar += y[i]*x[i];
        x_bar_carre += x[i]*x[i];
    }
    x_bar = x_bar / n;
    y_bar = y_bar / n;
    xy_bar = xy_bar / n;
    x_bar_carre = x_bar_carre / n;
    *a1 = (xy_bar - x_bar * y_bar) / (x_bar_carre - x_bar * x_bar);
    *a0 = (y_bar * x_bar_carre - x_bar * xy_bar) / (x_bar_carre - x_bar * x_bar);
}

void calcul_drt(float* x,float* y,int n){
    // a0 et a1, coefficients nécessaires pour tracer une courbe ensuite
    float a0=0;
    float a1=0;
    calcul_droite(x,y,n,&a0,&a1);

    //écriture des valeurs du jeu dans pointsdrt et des valeurs a0 et a1 dans drt
    char* txt1 = "drt.txt";
    char* txt2 = "pointsdrt.txt";
    ecriture_droite(txt1,txt2,x,y,a0,a1,n);
}

void calcul_exp(float *x, float* y, int n){
    // a0 et a1, coefficients nécessaires pour tracer une courbe ensuite
    float a0=0;
    float a1=0;
    float* y2 = malloc(n*sizeof(float));
    for(int i=0;i<n;++i){
        //ajustement avec le log 
        y2[i] = logf(y[i]);
    }
    calcul_droite(x,y2,n,&a0,&a1);
    free(y2);

    //écriture des valeurs du jeu dans pointsexp et des valeurs a0 et a1 dans exp
    char* txt1 = "exp.txt";
    char* txt2 = "pointsexp.txt";
    ecriture_droite(txt1,txt2,x,y,a0,a1,n);
}

void calcul_puissance(float *x, float *y, int n){
    // a0 et a1, coefficients nécessaires pour tracer une courbe ensuite
    float a0=0;
    float a1=0;
    float* y2 = malloc(n*sizeof(float));
    float* x2 = malloc(n*sizeof(float));
    for(int i=0;i<n;++i){
        //ajustement avec le log 
        y2[i] = logf(y[i]);
        x2[i] = logf(x[i]);
    }
    //régression classique sur les valeurs ajustées
    calcul_droite(x2,y2,n,&a0,&a1);
    free(y2);
    free(x2);

    //écriture des valeurs du jeu dans pointspuissance et des valeurs a0 et a1 dans puissance
    char* txt1 = "puissance.txt";
    char* txt2 = "pointspuissance.txt";
    ecriture_droite(txt1,txt2,x,y,a0,a1,n);
}

int main(int argc, char const *argv[])
{
    //Jeu d'essai 4.1
    float x41[20] = {0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38};
    float y41[20] = {0.99987 ,0.99997 ,1.00000 ,0.99997 ,0.99988 ,0.99973 ,0.99953, 0.99927 ,0.99897 ,0.99846 ,0.99805 ,0.999751 ,0.99705 ,0.99650 ,0.99664 ,0.99533 ,0.99472 ,0.99472 ,0.99333 ,0.99326};

    //Jeu d'essai 4.2
    float x42[21] = {752,855,871,734,610,582,921,492,569,462,907,643,862,524,679,902,918,828,875,809,894};
    float y42[21] = {85,83,162,79,81,83,281,81,81,80,243,84,84,82,80,226,260,82,186,77,223};

    //Jeu d'essai 4.3
    float x43[11] = {10,8,13,9,11,14,6,4,12,7,5};
    float y43[11] = {8.04,6.95,7.58,8.81,8.33,9.96,7.24,4.26,10.84,4.82,5.68};

    //Jeu d'essai 4.4
    float x44[10] = {88,89,90,91,92,93,94,95,96,97};
    float y44[10] = {5.89,6.77,7.87,9.11,10.56,12.27,13.92,15.72,17.91,22.13};

    //Jeu d'essai 4.5
    float x45[7] = {20,30,40,50,100,300,500};
    float y45[7] = {352,128,62.3,35.7,6.3,0.4,0.1};

    //Calcul régression
    calcul_drt(x43,y43,20);

    //Calcul ajustement exponentiel
    calcul_exp(x44,y44,10);

    //Calcul ajustement puissance
    calcul_puissance(x45,y45,7);

    return 0;
}
